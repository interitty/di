# Changelog #
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] ##

### Added ###

### Changed ###

### Fixed ###

## [1.0.10] - 2024-12-08 ##

### Changed ###

- PHP 8.3 property static types
- Upgrade dependent packages
- Upgrade license to 2025

## [1.0.9] - 2024-08-30 ##

### Changed ###

- Upgrade dependent packages

### Fixed ###

- Prevent incompatible nette/di version
- Phpstan extension.neon of composer/pcre missing by dg/composer-cleaner

## [1.0.8] - 2024-06-27 ##

### Changed ###

- Upgrade dependent packages

## [1.0.7] - 2024-05-19 ##

### Changed ###

- Update SECURITY key
- Upgrade dependent packages

## [1.0.6] - 2024-02-13 ##

### Changed ###
- Upgrade dependent packages

## [1.0.5] - 2023-12-28 ##

### Changed ###

- Increase minimal PHP to 8.3
- Update security contacts
- Upgrade dependent packages
- Upgrade license to 2024

## [1.0.4] - 2023-08-23 ##

### Added ###

- Literal factory

## [1.0.3] - 2023-03-12 ##

### Changed ###

- Update dependencies to newer version

### Fixed ###

- Minor changes due to new version of code checker

## [1.0.2] - 2023-01-30 ##

### Added ###

- Registration helpers `processRegisterAfter` and `processRegisterBefore`

## [1.0.1] - 2023-01-29 ##

### Added ###

- `setupServiceAlias` helper

## [1.0.0] - 2023-01-29 ##

### Added ###

- Basic package
