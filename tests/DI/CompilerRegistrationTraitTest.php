<?php

declare(strict_types=1);

namespace Interitty\DI;

use Interitty\PhpUnit\BaseIntegrationTestCase;
use Interitty\Utils\Arrays;
use Nette\DI\Compiler;
use Nette\DI\ContainerBuilder;
use PHPUnit\Framework\MockObject\MockObject;
use ReflectionClass;

/**
 * @coversDefaultClass Interitty\DI\CompilerRegistrationTrait
 */
class CompilerRegistrationTraitTest extends BaseIntegrationTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of register after processor implementation
     *
     * @return void
     * @covers ::processRegisterAfter
     * @group integration
     */
    public function testProcessRegisterAfter(): void
    {
        $name = 'name';
        $compiler = $this->createCompiler();
        $extension = $this->createCompilerExtensionMock();
        $extension->setCompiler($compiler, $name);

        $extensions = $compiler->getExtensions();
        self::assertArrayNotHasKey($name, $extensions);

        $this->callNonPublicMethod($extension, 'processRegisterAfter', ['services']);

        $registeredExtensions = $compiler->getExtensions();
        self::assertArrayHasKey($name, $registeredExtensions);
        Arrays::insertAfter($extensions, 'services', [$name => $extension]);

        self::assertSame($extensions, $registeredExtensions);
    }

    /**
     * Tester of register before processor implementation
     *
     * @return void
     * @covers ::processRegisterBefore
     * @group integration
     */
    public function testProcessRegisterBefore(): void
    {
        $name = 'name';
        $compiler = $this->createCompiler();
        $extension = $this->createCompilerExtensionMock();
        $extension->setCompiler($compiler, $name);

        $extensions = $compiler->getExtensions();
        self::assertArrayNotHasKey($name, $extensions);

        $this->callNonPublicMethod($extension, 'processRegisterBefore', ['services']);

        $registeredExtensions = $compiler->getExtensions();
        self::assertArrayHasKey($name, $registeredExtensions);
        Arrays::insertBefore($extensions, 'services', [$name => $extension]);

        self::assertSame($extensions, $registeredExtensions);
    }

    /**
     * Tester of ReflectionClass factory implementation
     *
     * @return void
     * @covers ::createReflectionClass
     * @group integration
     */
    public function testCreateReflectionCLass(): void
    {
        $extension = $this->createCompilerExtensionMock();
        $first = $this->callNonPublicMethod($extension, 'createReflectionClass', [$extension]);
        $second = $this->callNonPublicMethod($extension, 'createReflectionClass', [$extension]);
        self::assertInstanceOf(ReflectionClass::class, $first); // @phpstan-ignore-line
        self::assertInstanceOf(ReflectionClass::class, $second); // @phpstan-ignore-line
        self::assertNotSame($first, $second);
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Compiler factory
     *
     * @return Compiler
     */
    protected function createCompiler(): Compiler
    {
        $compiler = new Compiler();
        return $compiler;
    }

    /**
     * CompilerExtension mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return CompilerExtension|MockObject
     */
    protected function createCompilerExtensionMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(CompilerExtension::class, $methods);
        return $mock;
    }

    /**
     * ContainerBuilder mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return ContainerBuilder|MockObject
     */
    protected function createContainerBuilderMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(ContainerBuilder::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
