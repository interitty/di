<?php

declare(strict_types=1);

namespace Interitty\DI;

use Interitty\PhpUnit\BaseIntegrationTestCase;
use LogicException;
use Nette\Bootstrap\Configurator;
use Nette\DI\Compiler;
use Nette\DI\ContainerBuilder;
use Nette\DI\MissingServiceException;
use Nette\DI\ServiceCreationException;
use Nette\PhpGenerator\Literal;
use Nette\Utils\AssertionException;
use PHPUnit\Framework\MockObject\MockObject;

use function get_class;
use function sprintf;

/**
 * @coversDefaultClass Interitty\DI\CompilerExtension
 */
class CompilerExtensionTest extends BaseIntegrationTestCase
{
    // <editor-fold defaultstate="collapsed" desc="Integration tests">
    /**
     * Tester of simple integratio
     *
     * @return void
     * @covers ::loadConfiguration
     * @covers ::processConfig
     * @group integration
     */
    public function testSimpleIntegration(): void
    {
        $configContent = '
parameters:
    bar: bar
base:
    foo: %bar%
';
        $extension = $this->createCompilerExtensionMock();
        $this->callNonPublicMethod($extension, 'setDefaults', [['foo' => null]]);
        $configFile = $this->createTempFile($configContent, 'config.neon');
        $compile = static function (Configurator $sender, Compiler $compiler) use ($extension): void {
            self::assertFalse($sender->isDebugMode()); //Dumy use of property $sender
            $compiler->addExtension('base', $extension);
        };
        $configurator = $this->createConfigurator($configFile);
        $configurator->onCompile[] = $compile;
        $configurator->createContainer();
        self::assertSame(['foo' => 'bar'], $extension->getConfig());
    }

    /**
     * Tester of loadConfiguration exception
     *
     * @return void
     * @covers ::loadConfiguration
     * @covers ::processConfig
     * @group integration
     * @group negative
     */
    public function testLoadConfigurationException(): void
    {
        $configContent = '
extensions:
    base: %s

base:
    foo: bar
';
        $extension = $this->createCompilerExtensionMock();
        $extensionClass = get_class($extension);
        $configFile = $this->createTempFile(sprintf($configContent, $extensionClass), 'config.neon');
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Unknown configuration option ":extensionName › :option".');
        $this->expectExceptionData(['extensionName' => 'base', 'option' => 'foo', 'hint' => null]);
        $this->createContainer($configFile);
    }

    /**
     * Tester of getServiceDefinition implementation
     *
     * @return void
     * @covers ::getServiceDefinition
     * @group integration
     */
    public function testGetServiceDefinition(): void
    {
        $name = 'name';
        $containerBuilder = $this->createContainerBuilderMock();
        $serviceDefinition = $containerBuilder->addDefinition($name);
        $extension = $this->createCompilerExtensionMock();
        $result = $this->callNonPublicMethod($extension, 'getServiceDefinition', [$containerBuilder, $name]);
        self::assertSame($result, $serviceDefinition);
    }

    /**
     * Tester of getServiceDefinition exception implementation
     *
     * @return void
     * @covers ::getServiceDefinition
     * @group integration
     */
    public function testGetServiceDefinitionException(): void
    {
        $name = 'name';
        $containerBuilder = $this->createContainerBuilderMock();
        $extension = $this->createCompilerExtensionMock();
        $containerBuilder->addImportedDefinition($name);
        $this->expectException(MissingServiceException::class);
        $this->expectExceptionMessage('Service definition ":name" not found.');
        $this->expectExceptionData(['name' => 'name']);
        $this->callNonPublicMethod($extension, 'getServiceDefinition', [$containerBuilder, $name]);
    }

    /**
     * Tester of getServiceDefinition for FactoryDefinition implementation
     *
     * @return void
     * @covers ::getServiceDefinition
     * @group integration
     */
    public function testGetServiceDefinitionFactory(): void
    {
        $name = 'name';
        $containerBuilder = $this->createContainerBuilderMock();
        $serviceDefinition = $containerBuilder->addFactoryDefinition($name)->getResultDefinition();
        $extension = $this->createCompilerExtensionMock();
        $result = $this->callNonPublicMethod($extension, 'getServiceDefinition', [$containerBuilder, $name]);
        self::assertSame($result, $serviceDefinition);
    }

    /**
     * Tester of setupServiceAlias for registered service of given type
     *
     * @return void
     * @covers ::setupServiceAlias
     */
    public function testSetupServiceAliasRegistered(): void
    {
        $alias = 'alias';
        $serviceName = 'serviceName';
        $type = 'type';
        $containerBuilder = $this->createContainerBuilderMock(['addAlias', 'findByType']);
        $containerBuilder->expects(self::once())
            ->method('findByType')
            ->with(self::equalTo($type))
            ->willReturn([$serviceName => 'service']);
        $containerBuilder->expects(self::once())
            ->method('addAlias')
            ->with(self::equalTo($alias), self::equalTo($serviceName));

        $extension = $this->createCompilerExtensionMock(['getContainerBuilder', 'prefix']);
        $extension->expects(self::once())
            ->method('prefix')
            ->with(self::equalTo($alias))
            ->willReturn($alias);
        $extension->expects(self::once())
            ->method('getContainerBuilder')
            ->willReturn($containerBuilder);
        self::assertTrue($this->callNonPublicMethod($extension, 'setupServiceAlias', [$type, $alias]));
    }

    /**
     * Tester of setupServiceAlias for multiple registered service of given type
     *
     * @return void
     * @covers ::setupServiceAlias
     * @group negative
     */
    public function testSetupServiceAliasRegisteredMultiple(): void
    {
        $alias = 'alias';
        $type = 'type';
        $services = ['serviceName' => 'service', 'otherName' => 'service'];
        $containerBuilder = $this->createContainerBuilderMock(['addAlias', 'findByType']);
        $containerBuilder->expects(self::once())
            ->method('findByType')
            ->with(self::equalTo($type))
            ->willReturn($services);
        $containerBuilder->expects(self::never())
            ->method('addAlias');

        $extension = $this->createCompilerExtensionMock(['getContainerBuilder']);
        $extension->expects(self::once())
            ->method('getContainerBuilder')
            ->willReturn($containerBuilder);

        $this->expectException(ServiceCreationException::class);
        $this->expectExceptionMessage('Multiple services of type ":type" found');
        $this->expectExceptionData(['type' => $type]);
        $this->callNonPublicMethod($extension, 'setupServiceAlias', [$type, $alias]);
    }

    /**
     * Tester of setupServiceAlias for not registered service of given type
     *
     * @return void
     * @covers ::setupServiceAlias
     */
    public function testSetupServiceAliasNotegistered(): void
    {
        $alias = 'alias';
        $type = 'type';
        $services = [];
        $containerBuilder = $this->createContainerBuilderMock(['findByType']);
        $containerBuilder->expects(self::once())
            ->method('findByType')
            ->with(self::equalTo($type))
            ->willReturn($services);
        $extension = $this->createCompilerExtensionMock(['getContainerBuilder']);
        $extension->expects(self::once())
            ->method('getContainerBuilder')
            ->willReturn($containerBuilder);
        self::assertFalse($this->callNonPublicMethod($extension, 'setupServiceAlias', [$type, $alias]));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Unit tests">
    /**
     * Tester of Literal factory implementation
     *
     * @return void
     * @covers ::createLiteral
     */
    public function testCreateLiteral(): void
    {
        $value = 'return ?;';
        $arguments = [true];
        $extension = $this->createCompilerExtensionMock();
        $first = $this->callNonPublicMethod($extension, 'createLiteral', [$value, $arguments]);
        self::assertInstanceOf(Literal::class, $first); // @phpstan-ignore-line
        $second = $this->callNonPublicMethod($extension, 'createLiteral', [$value, $arguments]);
        self::assertInstanceOf(Literal::class, $second); // @phpstan-ignore-line
        self::assertNotSame($first, $second);
        self::assertSame('return true;', (string) $first);
    }

    /**
     * Tester of config getter and setter implementation
     *
     * @return void
     * @covers ::getConfig
     * @covers ::setConfig
     */
    public function testGetSetConfig(): void
    {
        $config = ['foo' => 'bar'];
        $extension = $this->createCompilerExtensionMock();
        $this->setNonPublicPropertyValue($extension, 'config', $config);
        self::assertSame($extension, $extension->setConfig($config));
        self::assertSame($config, $extension->getConfig());
    }

    /**
     * Tester of getConfig for object implementation
     *
     * @return void
     * @covers ::getConfig
     * @group negative
     */
    public function testGetConfigObject(): void
    {
        $extension = $this->createCompilerExtensionMock();
        $this->setNonPublicPropertyValue($extension, 'config', (object) []);
        $this->expectException(LogicException::class);
        $this->expectExceptionMessage('Config should be array.');
        $extension->getConfig();
    }

    /**
     * Tester of getConfigField implementation
     *
     * @return void
     * @covers ::getConfigField
     */
    public function testGetConfigField(): void
    {
        $value = 'value';
        $field = 'field';
        $extension = $this->createCompilerExtensionMock(['getConfig']);
        $extension->expects(self::once())
            ->method('getConfig')
            ->willReturn([$field => $value]);
        $result = $this->callNonPublicMethod($extension, 'getConfigField', [$field]);
        self::assertSame($value, $result);
    }

    /**
     * Tester of getConfigField exception implementation
     *
     * @return void
     * @covers ::getConfigField
     * @group negative
     */
    public function testGetConfigFieldException(): void
    {
        $extension = $this->createCompilerExtensionMock(['getConfig']);
        $extension->expects(self::once())
            ->method('getConfig')
            ->willReturn([]);
        $this->expectException(AssertionException::class);
        $this->expectExceptionMessage('Missing item "field" in config.');
        $this->callNonPublicMethod($extension, 'getConfigField', ['field']);
    }

    /**
     * Tester of deafults getter/setter implementation
     *
     * @return void
     * @covers ::getDefaults
     * @covers ::setDefaults
     */
    public function testGetSetDefaults(): void
    {
        $defaults = ['foo' => 'bar'];
        $this->processTestGetSet(CompilerExtension::class, 'defaults', $defaults);
    }

    /**
     * Tester of process config implementation
     *
     * @return void
     * @covers ::processConfig
     */
    public function testProcessConfig(): void
    {
        $config = ['foo' => '%bar%'];
        $defaults = ['foo' => null, 'bar' => null];
        $expandedConfig = ['foo' => 'bar', 'bar' => null];
        $containerBuilder = $this->createContainerBuilderMock();
        $containerBuilder->parameters = ['bar' => 'bar'];
        $extension = $this->createCompilerExtensionMock(['getContainerBuilder', 'getDefaults']);
        $extension->expects(self::once())
            ->method('getContainerBuilder')
            ->willReturn($containerBuilder);
        $extension->expects(self::once())
            ->method('getDefaults')
            ->willReturn($defaults);
        $this->setNonPublicPropertyValue($extension, 'config', $config);

        self::assertSame($expandedConfig, $this->callNonPublicMethod($extension, 'processConfig'));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * CompilerExtension mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return CompilerExtension|MockObject
     */
    protected function createCompilerExtensionMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(CompilerExtension::class, $methods);
        return $mock;
    }

    /**
     * ContainerBuilder mock factory
     *
     * @param string[] $methods [OPTIONAL] List of mocked method names
     * @return ContainerBuilder|MockObject
     */
    protected function createContainerBuilderMock(array $methods = []): MockObject
    {
        $mock = $this->createPartialMock(ContainerBuilder::class, $methods);
        return $mock;
    }

    // </editor-fold>
}
