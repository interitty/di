<?php

declare(strict_types=1);

namespace Interitty\DI;

use Interitty\Exceptions\Exceptions;
use Interitty\Utils\Validators;
use LogicException;
use Nette\DI\CompilerExtension as NetteCompilerExtension;
use Nette\DI\ContainerBuilder;
use Nette\DI\Definitions\FactoryDefinition;
use Nette\DI\Definitions\ServiceDefinition;
use Nette\DI\Helpers as DIHelpers;
use Nette\DI\MissingServiceException;
use Nette\DI\ServiceCreationException;
use Nette\PhpGenerator\Literal;
use Nette\Schema\Helpers as SchemaHelpers;
use Nette\Utils\Helpers;

use function array_diff_key;
use function array_keys;
use function assert;
use function is_array;
use function key;
use function next;

abstract class CompilerExtension extends NetteCompilerExtension
{
    use CompilerRegistrationTrait;

    /** @var mixed[] */
    protected $defaults = [];

    /**
     * @inheritdoc
     */
    public function loadConfiguration(): void
    {
        $config = $this->processConfig();
        $this->setConfig($config);
    }

    // <editor-fold defaultstate="collapsed" desc="Helpers">
    /**
     * Literal factory
     *
     * @param mixed[]|null $arguments [OPTIONAL]
     * @return Literal
     */
    protected function createLiteral(string $value, ?array $arguments = null): Literal
    {
        $literal = new Literal($value, $arguments);
        return $literal;
    }

    /**
     * Config processor
     *
     * Can be extended to add more validation, when some config value is wrong, like following:
     *
     * <code>
     * $config = parent::processConfig();
     * assert(Validators::check($config[self::CONFIG_BASE_PATH], 'string', self::CONFIG_BASE_PATH));
     * return $config;
     * </code>
     *
     * @return mixed[]
     */
    protected function processConfig(): array
    {
        $builder = $this->getContainerBuilder();
        $defaults = $this->getDefaults();
        $extra = array_diff_key((array) $this->config, $defaults);
        if ($extra !== []) {
            throw Exceptions::extend(LogicException::class)
                    ->addData('extensionName', $this->name)
                    ->addData('option', $option = key($extra))
                    ->addData('hint', $hint = Helpers::getSuggestion(array_keys($defaults), $option))
                    ->setMessage('Unknown configuration option ":extensionName › :option"'
                        . ($hint === null ? '.' : 'Did you mean ":extensionName › :hint"?'));
        }
        $config = SchemaHelpers::merge($this->config, $defaults);
        $expandedConfig = (array) DIHelpers::expand($config, $builder->parameters);
        return $expandedConfig;
    }

    /**
     * Service alias setup helper
     *
     * @param string $type
     * @param string $alias
     * @return bool
     * @throws ServiceCreationException
     */
    protected function setupServiceAlias(string $type, string $alias): bool
    {
        $builder = $this->getContainerBuilder();
        $services = $builder->findByType($type);
        $serviceName = key($services);
        $isServiceAliasAdded = false;
        if ($serviceName !== null) {
            if (next($services) !== false) {
                throw Exceptions::extend(ServiceCreationException::class)
                        ->setMessage('Multiple services of type ":type" found')
                        ->addData('type', $type);
            }
            $builder->addAlias($this->prefix($alias), (string) $serviceName);
            $isServiceAliasAdded = true;
        }
        return $isServiceAliasAdded;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters & Setters">
    /**
     * Extension configuration getter
     *
     * @return mixed[]
     */
    public function getConfig(): array
    {
        $config = parent::getConfig();
        if (is_array($config) !== true) {
            throw Exceptions::extend(LogicException::class)
                    ->setMessage('Config should be array.');
        }
        return $config;
    }

    /**
     * Extension configuration setter
     *
     * @param mixed[]|object  $config
     * @return static Provides fluent interface
     */
    public function setConfig($config): static
    {
        parent::setConfig((array) $config);
        return $this;
    }

    /**
     * Config field getter
     *
     * @param string $field
     * @return mixed
     */
    protected function getConfigField(string $field)
    {
        $config = $this->getConfig();
        assert(Validators::checkField($config, $field, null, 'item "%" in config'));
        return $config[$field];
    }

    /**
     * Defaults getter
     *
     * @return mixed[]
     */
    protected function getDefaults(): array
    {
        return $this->defaults;
    }

    /**
     * Defaults setter
     *
     * @param mixed[] $defaults
     * @return static Provides fluent interface
     */
    protected function setDefaults(array $defaults)
    {
        $this->defaults = $defaults;
        return $this;
    }

    /**
     * Service definition getter
     *
     * @param ContainerBuilder $builder
     * @param string $name
     * @return ServiceDefinition
     */
    protected function getServiceDefinition(ContainerBuilder $builder, string $name): ServiceDefinition
    {
        $definition = $builder->getDefinition($name);
        if ($definition instanceof FactoryDefinition) {
            $definition = $definition->getResultDefinition();
        }
        if (($definition instanceof ServiceDefinition) !== true) {
            throw Exceptions::extend(MissingServiceException::class)
                    ->setMessage('Service definition ":name" not found.')
                    ->addData('name', $name);
        }
        return $definition;
    }

    // </editor-fold>
}
