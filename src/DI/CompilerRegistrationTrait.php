<?php

declare(strict_types=1);

namespace Interitty\DI;

use Interitty\Utils\Arrays;
use ReflectionClass;

trait CompilerRegistrationTrait
{
    /**
     * Register given DI extension right after the already existed one
     *
     * @param string $afterName
     * @return void
     */
    protected function processRegisterAfter(string $afterName): void
    {
        $extensions = $this->compiler->getExtensions();
        Arrays::insertAfter($extensions, $afterName, [$this->name => $this]);

        $reflectionClass = $this->createReflectionClass($this->compiler);
        $extensionsProperty = $reflectionClass->getProperty('extensions');
        $extensionsProperty->setAccessible(true);
        $extensionsProperty->setValue($this->compiler, $extensions);
    }

    /**
     * Register given DI extension right before the already existed one
     *
     * @param string $afterName
     * @return void
     */
    protected function processRegisterBefore(string $afterName): void
    {
        $extensions = $this->compiler->getExtensions();
        Arrays::insertBefore($extensions, $afterName, [$this->name => $this]);

        $reflectionClass = $this->createReflectionClass($this->compiler);
        $extensionsProperty = $reflectionClass->getProperty('extensions');
        $extensionsProperty->setAccessible(true);
        $extensionsProperty->setValue($this->compiler, $extensions);
    }

    /**
     * ReflectionClass factory
     *
     * @phpstan-param class-string<RealInstanceType>|RealInstanceType $object
     * @phpstan-return ReflectionClass<RealInstanceType>
     * @template RealInstanceType of object
     */
    protected function createReflectionClass($object): ReflectionClass
    {
        $class = new ReflectionClass($object);
        return $class;
    }
}
